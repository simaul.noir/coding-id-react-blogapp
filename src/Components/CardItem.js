import MoreVertIcon from '@mui/icons-material/MoreVert';
import Avatar from '@mui/material/Avatar';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import { red } from '@mui/material/colors';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import PropTypes from 'prop-types';
import * as React from 'react';

export default function CardItem({ author, title, img }) {
  return (
    <Card sx={{ maxWidth: 345, mx: {xs : 'auto'} }}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
            {author.toLowerCase()[0]}
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title={title}
        subheader="September 14, 2016"
      />
      <CardMedia
        component="img"
        height="194"
        image={img}
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget ultricies odio. Donec non velit lacus. Maecenas interdum ultrices eros, vitae egestas justo vestibulum vel. Aliquam libero lectus, cursus id vestibulum quis, finibus sit amet purus. In in nisl turpis. Curabitur aliquam eleifend odio. Cras vulputate quis mi ac dapibus. Curabitur massa mauris, sollicitudin eu aliquam consectetur, laoreet eget urna. Donec ac tellus nec augue porta suscipit. Nunc sodales faucibus imperdiet. Mauris lorem erat, tristique sed neque eu, faucibus sollicitudin sem. Duis urna nisl, vehicula vulputate venenatis non, venenatis eget odio. 
        </Typography>
      </CardContent>
    </Card>
  );
}

CardItem.propTypes = {
  author: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  img: PropTypes.string.isRequired
}