import React from 'react'
import CardItem from './CardItem'
import PropTypes, { object } from 'prop-types'

export default function CardList({ posts }) {
  return(
    <>
      {
        posts.map((post, i) => (
          <CardItem author={post.author} title={post.title} img={post.img} key={i} id={i} />
        ))
      }
    </>
  )
}

CardList.propTypes = {
  posts: PropTypes.arrayOf(object).isRequired
}