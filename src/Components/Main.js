/* eslint-disable no-unused-vars */
import { Box } from '@mui/material';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { data, host } from '../utils/data';
import CardList from './CardList';
import SideBar from './SideBar';
import { useAsync } from './hooks';

function fetchData(host) {
  axios.get(host)
      .then((res) => {
        return res.data
      })
      .catch((res) => {
        console.log(res);
      })
}

export default function Main() {
  const { execute, status, value, error } = useAsync(fetchData(host), false);

  return (
    <Box sx={{maxWidth: '1200px', mx: 'auto', my: 4, display: 'flex', flexDirection: 'row', gap: 2}}>
      <Box sx={{flexGrow: 3, flexWrap: 'wrap', gap: 4, flexDirection: {  xs: 'column', sm: 'row', md: 'row' }, display: 'flex', justifyContent: 'center', mx: 'auto'}}>
        <CardList posts={data} />
      </Box>
      <Box sx={{ flexGrow: 1, display: { xs: 'none',  md : 'flex'} }}> 
        <SideBar />
      </Box>
    </Box>
  )
}
